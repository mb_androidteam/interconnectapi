<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'v1'], function () {
	Route::post('/signin', 'AuthController@register');
	Route::get('/logout', 'AuthController@logout');
	Route::get('/refreshtoken', 'AuthController@refreshToken');
	Route::post('/updatelocation', 'LocationController@updateLocation');
	Route::get('/getuserlocation', 'LocationController@userLocationHistory');
	Route::get('/getnearlocation', 'LocationController@getNearby');
	Route::get('/getmessagetype', 'MessageController@getMessageType');
	Route::post('/sendmessage', 'MessageController@sendMessage');
	Route::post('/sendreply', 'MessageController@sendReplyMessage');
	Route::get('/getmessage', 'MessageController@getMessageDetails');
	Route::get('/getmessagebyplace', 'MessageController@getMessageDetailsByPlace');
	Route::get('/test', function (Illuminate\Http\Request $request) {
		echo $request->get('test');
	});
});
