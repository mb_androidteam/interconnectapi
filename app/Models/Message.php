<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {
	protected $table = 'message';
	protected $primaryKey = 'MessageId';
	const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';
	protected $fillable = array('MessageTypeId');
	public function messageType() {
		return $this->belongsTo('App\Models\MessageType', 'MessageTypeId', 'MessageTypeId');
	}
	public function user() {
		return $this->belongsTo('App\Models\User', 'SenderId', 'UserId');
	}
	public function getMessageType() {
		return $this->belongsTo('App\Models\MessageType', 'MessageTypeId', 'MessageTypeId')->select('MessageType', 'PostiveResponse', 'NegetiveResponse');
	}
	public function getUser() {
		return $this->belongsTo('App\Models\User', 'SenderId', 'UserId')->select('UserId', 'FirstName');
	}
}