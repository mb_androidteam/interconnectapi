<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model {
	protected $table = 'userlocation';
	protected $primaryKey = 'UserLocationId';
	const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';
	protected $fillable = array('UserId');

	public function user() {
		return $this->belongsTo('App\Models\User', 'UserId', 'UserId');
	}

}