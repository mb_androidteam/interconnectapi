<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Location extends Model {
	protected $table = 'location';
	protected $primaryKey = 'LocationId';
	const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';
	protected $fillable = array('LocationId');
}