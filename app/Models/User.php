<?php
namespace App\Models;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable {
	use AuthenticableTrait;
	protected $table = 'user';
	protected $primaryKey = 'UserId';
	const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';
	protected $fillable = array('UserId');
}