<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class MessageResponse extends Model {
	protected $table = 'messageresponse';
	protected $primaryKey = 'MessageResponseId';
	const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';
	protected $fillable = array('MessageId');
	public function message() {
		return $this->belongsTo('App\Models\Message', 'MessageId', 'MessageId');
	}
	public function user() {
		return $this->belongsTo('App\Models\User', 'RecieverId', 'UserId');
	}
}