<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class MessageType extends Model {
	protected $table = 'messagetype';
	protected $primaryKey = 'MessageTypeId';
	const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';
}