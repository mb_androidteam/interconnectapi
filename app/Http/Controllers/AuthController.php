<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Input;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use Validator;

class AuthController extends Controller {
	/*
		|--------------------------------------------------------------------------
		| Authentication Controller
		|--------------------------------------------------------------------------
		|
		| This controller handles the registration of new users, as well as the
		| authentication of existing users.
		| @created by : Abhijith A Nair
		| @created on : 15-02-2017
		| @modified by: Abhijith A Nair
		| @modified on: 20-02-2017
	*/

	/**
	 * Illuminate\Http\Request object
	 */

	private $request;
	/**
	 * Create a new authentication controller instance.
	 * @return void
	 */

	public function __construct(Request $request) {
		$this->request = $request;

	}

	/**
	 * [register is used to check whether user is already registred  else register the user]
	 * @return [array] [user details]
	 */

	public function register() {
		try {
			// Validation rules
			$rules = [
				'appVersion' => 'required',
				'apiVersion' => 'required',
				'firstName' => 'required',
				'Email' => 'required|email',
				'longitude' => 'numeric',
				'latitude' => 'numeric',
				'fcmid' => 'required',
			];
			// Validate the form data aganist the rules
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'resultCode' => 0,
					'error' => $validator->errors(),
				]);
			}
			//get credentials into variables
			$email = $this->request->get('Email');
			$user = User::where('Email', $email)->first();

			if ($user) {
				$token = JWTAuth::fromUser($user);
				$longitude = $this->request->get('longitude');
				$latitude = $this->request->get('latitude');
				$place = $this->request->get('place');
				$location = $this->request->get('location');
				$profileImage = $this->request->get('profileImage');
				$fcmId = $this->request->get('fcmid');
				$deviceId = $this->request->get('userDeviceId');
				$user->DeviceId = $deviceId ? $deviceId : $user->DeviceId;
				$user->Longitude = $longitude ? $longitude : $user->Longitude;
				$user->Latitude = $latitude ? $latitude : $user->Latitude;
				$user->CurrentPlace = $place ? $place : $user->CurrentPlace;
				$user->CurrentLocation = $location ? $location : $user->CurrentLocation;
				$user->ProfileImage = $profileImage ? $profileImage : $user->ProfileImage;
				$user->ProfileImage = $profileImage ? $profileImage : $user->ProfileImage;
				$user->FcmId = $fcmId ? $fcmId : $user->FcmId;
				$user->save();
				$authToken = $token;
				$responseArr = [
					'apiVersion' => '1.0',
					'resultCode' => 1,
					'authToken' => $authToken,
					'userId' => $user->UserId,
					'firstName' => $user->FirstName,
					'lastName' => $user->LastName ? $user->LastName : "",
					'userEmail' => $user->Email,
					'userGender' => $user->Gender ? $user->Gender : "",
					'ProfileImage' => $user->ProfileImage ? $user->ProfileImage : "",
					'longitude' => $user->Longitude ? $user->Longitude : "",
					'latitude' => $user->Latitude ? $user->Latitude : "",
				];
				return response()->json($responseArr, 200);
			} else {
				// Register the new user
				$rules = [
					'Email' => 'unique:user,Email',
				];
				// Validate the form data aganist the rules
				$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
					return response()->json([
						'resultCode' => 0,
						'error' => $validator->errors(),
					]);
				}
				$user = new User();
				$user->FirstName = $this->request->get('firstName');
				$user->LastName = $this->request->get('lastName');
				$user->Email = $this->request->get('Email');
				$user->Gender = $this->request->get('userGender');
				$user->Password = Hash::make($this->request->get('userPassword'));
				$user->DeviceId = $this->request->get('userDeviceId');
				$user->Longitude = $this->request->get('longitude');
				$user->Latitude = $this->request->get('latitude');
				$user->CurrentPlace = $this->request->get('place');
				$user->CurrentLocation = $this->request->get('location');
				$user->ProfileImage = $this->request->get('profileImage');
				$user->FcmId = $this->request->get('fcmid');
				$user->Status = 1;
				$user->save();
				$authToken = JWTAuth::fromUser($user);
				$responseArr = [
					'apiVersion' => '1.0',
					'resultCode' => 1,
					'authToken' => $authToken,
					'userId' => $user->UserId,
					'firstName' => $user->FirstName,
					'lastName' => $user->LastName ? $user->LastName : "",
					'userEmail' => $user->Email,
					'userGender' => $user->Gender ? $user->Gender : "",
					'ProfileImage' => $user->ProfileImage ? $user->ProfileImage : "",
					'longitude' => $user->Longitude ? $user->Longitude : "",
					'latitude' => $user->Latitude ? $user->Latitude : "",
				];
				return response()->json($responseArr, 200);
			}

		} catch (JWTException $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}

	}
	/**
	 * [logout and invalidate token]
	 * @return [json object] [success or not]
	 */
	public function logout() {
		try {
			JWTAuth::invalidate(JWTAuth::getToken());
			return response()->json([
				'resultCode' => 1,
				'message' => 'logout successfully',
			], 200);
		} catch (JWTException $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
	/**
	 * [token refresh if the token is expired]
	 * @return [json object] [new token]
	 */
	public function refreshToken() {
		try {
			$oldtoken = JWTAuth::getToken();
			$token = JWTAuth::refresh($oldtoken);
			return response()->json([
				'resultCode' => 1,
				'authToken' => $token,
			], 200);
		} catch (JWTException $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}

	}
}