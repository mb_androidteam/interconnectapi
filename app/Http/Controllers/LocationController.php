<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Models\User;
use App\Models\UserLocation;
use DB;
use Illuminate\Http\Request;
use Input;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use Validator;

class LocationController extends Controller {
	/*
		|--------------------------------------------------------------------------
		| Location Controller
		|--------------------------------------------------------------------------
		|
		| This controller handles the locations of users
		| @created by : Abhijith A Nair
		| @created on : 16-02-2017
		| @modified by: Abhijith A Nair
		| @modified on: 17-02-2017
	*/

	/**
	 * Illuminate\Http\Request object
	 */

	private $request;
	/**
	 * Create a new authentication controller instance.
	 * @return void
	 */

	public function __construct(Request $request) {
		$this->request = $request;
		$this->middleware('jwt.auth');
	}
	/**
	 * [updatelocation used for user location updated when he moves]
	 * @return [json array] [status of updated or not]
	 */
	public function updateLocation() {
		try {
			//Validation Rules
			$rules = [
				'appVersion' => 'required',
				'apiVersion' => 'required',
				'longitude' => 'required|numeric',
				'latitude' => 'required|numeric',
				'location' => 'required',
				'place' => 'required',
			];
			// Validate the form data aganist the rules
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'resultCode' => 0,
					'error' => $validator->errors(),
				], 500);
			}
			$user = JWTAuth::toUser();
			if (!$user) {
				return response()->json([
					'resultCode' => 0,
					'error' => 'user not found',
				], 404);
			} else {
				$longitude = $this->request->get('longitude');
				$latitude = $this->request->get('latitude');
				$userlocation = new UserLocation();
				$userlocation->Longitude = $longitude;
				$userlocation->Latitude = $latitude;
				$userlocation->UserId = $user->UserId;
				$userlocation->Location = $this->request->get('location');
				$userlocation->Place = $this->request->get('place');
				$result = $userlocation->save();
				$updateuser = User::where('UserId', $user->UserId)->first();
				$updateuser->Longitude = $longitude;
				$updateuser->Latitude = $latitude;
				$updateuser->CurrentLocation = $this->request->get('location');
				$updateuser->CurrentPlace = $this->request->get('place');
				$updateuser->save();
				$location = new Location();
				$location->Longitude = $longitude;
				$location->Latitude = $latitude;
				$location->Location = $this->request->get('location');
				$location->Place = $this->request->get('place');
				$isSaved = $this->saveLocation($location);
				if ($result) {
					return response()->json([
						'resultCode' => 1,
						'message' => 'user location  updated',
					], 200);
				} else {
					return response()->json([
						'resultCode' => 0,
						'message' => 'user location not updated',
					], 200);
				}

			}

		} catch (JWTException $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);

		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);

		}
	}
	/**
	 * [saveLocation is used for save location in database]
	 * @param  [Location Model] $location [details of location]
	 * @return [boolean]           [Success or not]
	 */
	public function saveLocation($location) {
		try {
			$saved = $this->checkLocation($location);
			if (!$saved) {
				$result = $location->save();
				return $result;
			} else {
				$lon1 = $saved->Longitude;
				$lat1 = $saved->Latitude;
				$lon2 = $location->Longitude;
				$lat2 = $location->Latitude;
				$unit = "M";
				$distance = $this->calculateDistance($lat1, $lon1, $lat2, $lon2, $unit);
				if ($distance > 100 && $saved->Place != $location->Place) {
					$result = $location->save();
					return $result;
				} else {
					return false;
				}
			}
		} catch (Exception $e) {
			return false;
		}
	}
	/**
	 * [checkLocation saved in database or not]
	 * @param  [Location Model] $location [details of location]
	 * @return [Location Model]           [Already saved location details or false]
	 */
	public function checkLocation($location) {
		try {
			$location = Location::where('Location', $location->Location)->first();
			if ($location) {
				return $location;
			} else {
				return false;
			}
		} catch (Exception $e) {
			return false;
		}
	}
	/**
	 * [caculate distance between two locations]
	 * @param  [float] $lat1 [latitude 1]
	 * @param  [float] $lon1 [longitude 1]
	 * @param  [float] $lat2 [latitude 2]
	 * @param  [float] $lon2 [longitude 2]
	 * @param  [char] $unit [unit k-km, m-meter]
	 * @return [float]       [distance between two locations]
	 */
	public function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit) {
		try {
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);
			if ($unit == "K") {
				return ($miles * 1.609344);
			} else if ($unit == "N") {
				return ($miles * 0.8684);
			} else if ($unit == "M") {
				return ($miles * 1609.34);
			} else {
				return $miles;
			}
		} catch (Exception $e) {
			return 0;
		}
	}
	/**
	 * [track user locations]
	 * @return [json array] [user location details]
	 */
	public function userLocationHistory() {
		try {
			$user = JWTAuth::toUser();
			if (!$user) {
				return response()->json([
					'resultCode' => 0,
					'error' => 'user not found',
				], 404);
			} else {
				$userlocations = UserLocation::where('UserId', $user->UserId)->orderBy('CreatedOn', 'DESC')->get()->toArray();
				return response()->json([
					'resultCode' => 1,
					'locationHistory' => $userlocations,
				], 200);
				echo json_encode($userlocations);
			}
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
	/**
	 * [get near by locations and users]
	 * @return [json array] [locations and user details]
	 */
	public function getNearby() {
		try {

			$rules = [
				'appVersion' => 'required',
				'apiVersion' => 'required',
				'longitude' => 'required|numeric',
				'latitude' => 'required|numeric',
				'distance' => 'required|numeric',
			];
			// Validate the form data aganist the rules
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'resultCode' => 0,
					'error' => $validator->errors(),
				], 500);
			}
			$longitude = $this->request->get('longitude');
			$latitude = $this->request->get('latitude');
			$distance = $this->request->get('distance');
			$locations = DB::select('call nearbylocation(?,?,?)', array($longitude, $latitude, $distance));
			foreach ($locations as $loc) {
				$location = $loc->Location;
				$place = $loc->Place;
				$matchUserPlace = ['CurrentLocation' => $location, 'CurrentPlace' => $place];
				$users = User::where($matchUserPlace)->get();
				$locationDetails[] = [
					'LocationId' => $loc->LocationId,
					'Location' => $loc->Location,
					'Place' => $loc->Place,
					'Longitude' => $loc->Longitude,
					'Latitude' => $loc->Latitude,
					'CreatedOn' => $loc->CreatedOn,
					'UpdatedOn' => $loc->UpdatedOn,
					'Distance' => $loc->distance,
					'NoOfPeoples' => $users->count(),
					'Peoples' => $users,
				];
			}
			if ($locations) {
				return response()->json([
					'resultCode' => 1,
					'locations' => $locationDetails,
				], 200);
			} else {
				return response()->json([
					'resultCode' => 0,
					'locations' => [],
					'message' => 'No location found',
				], 500);
			}
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
}