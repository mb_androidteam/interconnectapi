<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\MessageResponse;
use App\Models\MessageType;
use App\Models\User;
use FCM;
use Illuminate\Http\Request;
use Input;
use JWTAuth;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Validator;

class MessageController extends Controller {
	/*
		|--------------------------------------------------------------------------
		| Message Controller
		|--------------------------------------------------------------------------
		|
		| This controller handles the message of users
		| @created by : Abhijith A Nair
		| @created on : 18-02-2017
		| @modified by: Abhijith A Nair
		| @modified on: 20-02-2017
	*/

	/**
	 * Illuminate\Http\Request object
	 */

	private $request;
	/**
	 * Create a new authentication controller instance.
	 * @return void
	 */

	public function __construct(Request $request) {
		$this->request = $request;
		$this->middleware('jwt.auth');
	}
	/**
	 * [get all message types from database]
	 * @return [json array] [response array]
	 */
	public function getMessageType() {
		try {

			$messageType = MessageType::all();
			return response()->json([
				'resultCode' => 1,
				'messagetype' => $messageType,
			], 200);
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
	/**
	 * [send Messages to the users who is in the location ]
	 * @return [json object] [response]
	 */
	public function sendMessage() {
		try {
			//Validation Rules
			$rules = [
				'appVersion' => 'required',
				'apiVersion' => 'required',
				'longitude' => 'required|numeric',
				'latitude' => 'required|numeric',
				'place' => 'required',
				'messagetype' => 'required|numeric',
				'subject' => 'required',
				'description' => 'required',
				'location' => 'required',
			];
			// Validate the form data aganist the rules
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'resultCode' => 0,
					'error' => $validator->errors(),
				], 500);
			}
			$user = JWTAuth::toUser();
			if (!$user) {
				return response()->json([
					'resultCode' => 0,
					'error' => 'user not found',
				], 404);
			}
			$place = $this->request->get('place');
			$location = $this->request->get('location');
			$message = new Message();
			$message->SenderId = $user->UserId;
			$message->Subject = $this->request->get('subject');
			$message->Description = $this->request->get('description');
			$message->ReplyTypeId = $this->request->get('messagetype');
			$message->Place = $place;
			$message->Location = $this->request->get('location');
			$message->Longitude = $this->request->get('longitude');
			$message->Latitude = $this->request->get('latitude');
			$message->IsSent = 1;
			$isSaved = $message->save();
			$matchThese = ['CurrentLocation' => $location, 'CurrentPlace' => $place];
			$users = User::where($matchThese)->get();
			foreach ($users as $user) {
				$messageReponse = new MessageResponse();
				$messageReponse->MessageId = $message->MessageId;
				$messageReponse->RecieverId = $user->UserId;
				$messageReponse->Response = "";
				$messageReponse->IsRead = 0;
				$messageReponse->IsReadBySender = 0;
				$messageReponse->ReplyMessage = "";
				// $fcmId = $user->FcmId;
				$result = $this->sendUserMessage($messageReponse);
			}
			/*
				TODO - SEND Firebase notification
			*/
			//send notification to user;
			//
			$fcmToken = collect($users)->pluck('FcmId')->all();
			// $fcmToken = $fcmToken->toArray();
			$messageId = $message->MessageId;
			$notificationData = array('messageId' => $messageId,
				'notificationType' => 1);
			$title = "You got a message";
			$body = "Please response on your message";
			if ($fcmToken) {
				$notificationResponse = $this->sendNotification($title, $body, $notificationData, $fcmToken);
			}
			if ($isSaved) {
				return response()->json([
					'resultCode' => 1,
					'message' => 'message send successfully',
				], 200);
			} else {
				return response()->json([
					'resultCode' => 1,
					'error' => 'message send failed',
				], 500);
			}

		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
	/**
	 * [mesage details of the user saved to database]
	 * @param  [MessageResponse object] $messageReponse [save response row]
	 * @return [boolean]                 [saved or not]
	 */
	public function sendUserMessage($messageReponse) {
		try {
			$isSaved = $messageReponse->save();
			return $isSaved;
		} catch (Exception $e) {
			return false;
		}
	}
	/**
	 * [send reply to the message recieved]
	 * @return [json object] [response]
	 */
	public function sendReplyMessage() {
		try {
			//Validation Rules
			$rules = [
				'appVersion' => 'required',
				'apiVersion' => 'required',
				'messageid' => 'required|numeric',
				'messageresponse' => 'required',
				'messagereply' => 'required',
			];
			// Validate the form data aganist the rules
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'resultCode' => 0,
					'error' => $validator->errors(),
				], 500);
			}
			$user = JWTAuth::toUser();
			if (!$user) {
				return response()->json([
					'resultCode' => 0,
					'error' => 'user not found',
				], 404);
			}
			$messageId = $this->request->get('messageid');
			$matchThese = ['RecieverId' => $user->UserId, 'MessageId' => $messageId];
			$messageResponse = MessageResponse::where($matchThese)->first();
			if ($messageResponse) {
				$messageResponse->Response = $this->request->get('messageresponse');
				$messageResponse->ReplyMessage = $this->request->get('messagereply');
				$messageResponse->IsRead = 1;
				$result = $messageResponse->save();
				if ($result) {
					$message = Message::where('MessageId', $messageId)->first();
					$senderId = $message->SenderId;
					$senderDetails = User::where('UserId', $senderId)->first();
					/* TODO - Implement Firebase */
					$notificationData = array('messageId' => $messageId,
						'notificationType' => 2);
					$title = "You got a reply";
					$body = "Some one give response on your message";
					$fcmToken = $senderDetails->FcmId;
					if ($fcmToken) {
						$notificationResponse = $this->sendNotification($title, $body, $notificationData, $fcmToken);
					}

					return response()->json([
						'resultCode' => 1,
						'message' => 'response send successfully',
					], 200);
				} else {
					return response()->json([
						'resultCode' => 0,
						'error' => 'response send failed',
					], 500);
				}
			} else {
				return response()->json([
					'resultCode' => 0,
					'error' => 'no message found',
				], 500);
			}

		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
	/**
	 * [get message details of message using messageid]
	 * @return [json array] [details of a message]
	 */
	public function getMessageDetails() {
		try {
			$rules = [
				'appVersion' => 'required',
				'apiVersion' => 'required',
				'messageid' => 'required|numeric',
			];
			// Validate the form data aganist the rules
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'resultCode' => 0,
					'error' => $validator->errors(),
				], 500);
			}
			$messageId = $this->request->get('messageid');
			$message = Message::where('MessageId', $messageId)->first();
			if (!$message) {
				return response()->json([
					'resultCode' => 0,
					'error' => 'no message found',
				], 500);
			}
			$isRead = 1;
			$matchThese = ['MessageId' => $messageId, 'IsRead' => $isRead];

			// $messageResponse = MessageResponse::where($matchThese)->get();
			$messageResponse = MessageResponse::select('messageresponse.MessageResponseId', 'messageresponse.MessageId', 'messageresponse.Response', 'messageresponse.IsRead', 'messageresponse.ReplyMessage', 'messageresponse.CreatedOn', 'messageresponse.UpdatedOn', 'user.FirstName', 'user.LastName', 'user.ProfileImage')->join('user', 'messageresponse.RecieverId', '=', 'user.UserId')->where($matchThese)->get();
			$location = $message->Location;
			$place = $message->Place;
			$messageTypeId = $message->ReplyTypeId;
			$messageType = MessageType::where('MessageTypeId', $messageTypeId)->first();
			$postiveResponse = $messageType->PostiveResponse;
			$matchUser = ['CurrentLocation' => $location, 'CurrentPlace' => $place];
			$users = User::where($matchUser)->get();
			$userCount = 0;
			$safeCount = 0;
			if ($messageResponse) {
				$filterResponse = collect($messageResponse);
				$filterMessageResponse = $filterResponse->where('Response', $postiveResponse);
				$safeCount = $filterMessageResponse->count();
			}
			if ($users) {
				$userCount = $users->count();
			}
			$responseArr = [
				'resultCode' => 1,
				'messageDetails' => $message,
				'messageResponse' => $messageResponse,
				'messageType' => $messageType,
				'postiveResponse' => $safeCount,
				'userCount' => $userCount,
			];
			return response()->json($responseArr, 200);
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
	/**
	 * [send notification to the device using fcm]
	 * @param  [string] $title    [title of notification]
	 * @param  [string] $body     [body of notification]
	 * @param  [array]  $data     [data send with the notification]
	 * @param  [string] $fcmToken [token value of fcm]
	 * @return [array]            [resoponse of notification send]
	 */
	public function sendNotification($title, $body, $data, $fcmToken) {
		try {
			$optionBuiler = new OptionsBuilder();
			$optionBuiler->setTimeToLive(60 * 20);
			$notificationBuilder = new PayloadNotificationBuilder($title);
			$notificationBuilder->setBody($body)
				->setSound('default');
			$dataBuilder = new PayloadDataBuilder();
			$dataBuilder->addData($data);
			$option = $optionBuiler->build();
			$notification = $notificationBuilder->build();
			$data = $dataBuilder->build();
			$downstreamResponse = FCM::sendTo($fcmToken, $option, $notification, $data);
			return $downstreamResponse;
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}

	}
	public function getMessageDetailsByPlace() {
		try {
			$rules = [
				'appVersion' => 'required',
				'apiVersion' => 'required',
				'place' => 'required',
				'longitude' => 'required',
				'latitude' => 'required',
				'location' => 'required',
			];
			// Validate the form data aganist the rules
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				return response()->json([
					'resultCode' => 0,
					'error' => $validator->errors(),
				], 500);
			}
			$user = JWTAuth::toUser();
			if (!$user) {
				return response()->json([
					'resultCode' => 0,
					'error' => 'user not found',
				], 404);
			}
			$userId = $user->UserId;
			$place = $this->request->get('place');
			$location = $this->request->get('location');
			$matchUserPlace = ['SenderId' => $userId, 'Place' => $place, 'Location' => $location];
			$message = Message::where($matchUserPlace)->orderBy('CreatedOn', 'DESC')->first();
			if (!$message) {
				return response()->json([
					'resultCode' => 0,
					'error' => 'no message found',
				], 500);
			}
			$messageId = $message->MessageId;
			$isRead = 1;
			$matchThese = ['messageresponse.MessageId' => $messageId, 'messageresponse.IsRead' => $isRead];
			$messageResponse = MessageResponse::select('messageresponse.MessageResponseId', 'messageresponse.MessageId', 'messageresponse.Response', 'messageresponse.IsRead', 'messageresponse.ReplyMessage', 'messageresponse.CreatedOn', 'messageresponse.UpdatedOn', 'user.FirstName', 'user.LastName', 'user.ProfileImage')->join('user', 'messageresponse.RecieverId', '=', 'user.UserId')->where($matchThese)->get();
			$messageTypeId = $message->ReplyTypeId;
			$messageType = MessageType::where('MessageTypeId', $messageTypeId)->first();
			$postiveResponse = $messageType->PostiveResponse;
			$matchUser = ['CurrentLocation' => $location, 'CurrentPlace' => $place];
			$users = User::where($matchUser)->get();
			$userCount = 0;
			$safeCount = 0;
			if ($messageResponse) {
				$filterResponse = collect($messageResponse);
				$filterMessageResponse = $filterResponse->where('Response', $postiveResponse);
				$safeCount = $filterMessageResponse->count();
			}
			if ($users) {
				$userCount = $users->count();
			}
			$responseArr = [
				'resultCode' => 1,
				'messageDetails' => $message,
				'messageResponse' => $messageResponse,
				'messageType' => $messageType,
				'postiveResponse' => $safeCount,
				'userCount' => $userCount,
			];
			return response()->json($responseArr, 200);
		} catch (Exception $e) {
			return response()->json([
				'resultCode' => 0,
				'error' => $e->getMessage(),
			], 500);
		}
	}
}